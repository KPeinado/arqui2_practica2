//Librerias para MPU
#include "I2Cdev.h"
#include "MPU6050.h"

//Incluir libreria si se cumple condición
#if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
    #include "Wire.h"
#endif

//Variable para lectura de MPU
MPU6050 mpu;

//Estos son los valores leidos en el setup y los cuales deberia mantener
int16_t ax0, ay0, az0;
int16_t gx0, gy0, gz0;
//Estos son los valores que van a ir cambiando
float ax, ay, az;
float gx, gy, gz;
float gsx, gsy, gsz;

//Liberia de los servos
#include <Servo.h>

//Se utilizarán 3 servos para los 3 ejes
Servo servoX, servoY, servoZ;

void printValues() {
    int dt = 75;
    //Imprime los valores en el puerto serial
    gsx += (gx + 2) * dt / 1000;
    ax0 = 90 - gsx;
    //ax0 = 90 + (90 * ax);
    servoX.write(ax0);

    gsy += (gy - 2) * dt / 1000;
    ay0 = 90 - gsy;
    //ay0 = 90 + (90 * ay);
    servoY.write(ay0);

    gsz += (gz + 1) * dt / 1000;
    az0 = 90 + gsz;
    servoZ.write(az0);
    
    Serial.print(ax); Serial.print("\t");
    Serial.print(ay); Serial.print("\t");
    Serial.print(az); Serial.print("\t");
    Serial.print(gx); Serial.print("\t");
    Serial.print(gy); Serial.print("\t");
    Serial.print(gz); Serial.print("\t");
    Serial.println();
}

void setup() {
    //Se utilizarán los pines D4, D7 y D8 para los servos X Y y Z
    servoX.attach(4);
    servoY.attach(7);
    servoZ.attach(8);
    gsx = gsy = gsz = 0;
    servoX.write(90);
    servoY.write(90);
    servoZ.write(90);

    //Los pines utilizados por el MPU son:
    //SCL: A5
    //SDA: A4
    //INT: D2
    
    //Inicializar MPU0
    #if I2CDEV_IMPLEMENTATION == I2CDEV_ARDUINO_WIRE
        Wire.begin();
    #elif I2CDEV_IMPLEMENTATION == I2CDEV_BUILTIN_FASTWIRE
        Fastwire::setup(400, true);
    #endif
    
    //Iniciar MPU
    Serial.begin(38400);
    Serial.println("Initializing I2C devices...");
    mpu.initialize();
    Serial.println("Testing device connections...");
    Serial.println(mpu.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
}

void loop() {
    // Leer acelerometro y giroscopio
    mpu.getMotion6(&ax0, &ay0, &az0, &gx0, &gy0, &gz0);

    ax = ax0/15900.0;
    ay = ay0/15900.0;
    az = az0/15900.0; 

    gx = gx0/131.0; 
    gy = gy0/131.0;
    gz = gz0/131.0;
    
    printValues();

    delay(50);
}
